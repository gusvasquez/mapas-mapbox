import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import Mapboxgl from 'mapbox-gl'; // or "const mapboxgl = require('mapbox-gl');"

Mapboxgl.accessToken = 'pk.eyJ1IjoiZ2F2YXNxdWV6IiwiYSI6ImNsY3prNWY4ZjA2OHUzcHBmeDY2ZjhudW0ifQ.vTWtskcjVNj6xIB6sqQg_A';


// geolocalizacion
// si el navegador no soporta la geolocalizacion no pasa a la renderizacion de la aplicacion
if (!navigator.geolocation) {
  alert('El Navegador no soporta la geolocalizacion');
  throw Error('El Navegador no soporta la geolocalizacion');
}


if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
