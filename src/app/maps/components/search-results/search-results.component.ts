import { Component, OnInit } from '@angular/core';
import { Feature } from '../../interfaces/places';
import { MapService, PlacesService } from '../../services';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent {

  public selectdId: string = '';

  constructor(private placesServices: PlacesService, private mapService: MapService) { }

  get isLoadingPlaces() {
    return this.placesServices.isLoadingPlaces;
  }

  get places() {
    return this.placesServices.places;
  }

  flyTo(place: Feature) {
    this.selectdId = place.id;
    const [lng, lat] = place.center;
    this.mapService.flyTo([lng, lat])
  }

  getDirections(place: Feature) {

    if (!this.placesServices.userLocation) throw Error('No hay userLocation');
    this.placesServices.deletePlaces();

    const start = this.placesServices.userLocation;
    const end = place.center as [number, number];

    this.mapService.getRouteBetwenPoints(start, end);
  }

}
