import { Component } from '@angular/core';
import { PlacesService } from '../../services';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent {

  private debounceTime?: NodeJS.Timeout;

  constructor(private placesService: PlacesService) { }



  onQueryChanged(query: string = '') {
    /* si tiene un valor se limpia */
    if (this.debounceTime) clearTimeout(this.debounceTime);

    /* emite el valor cada segundo que pasa */
    this.debounceTime = setTimeout(() => {
      console.log('mandar este query', query);
      this.placesService.getPlacesByQuery(query);
    }, 1000)
  }

}
