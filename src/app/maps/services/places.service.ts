import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PlacesApiClient } from '../api/placesApiClient';
import { Feature, PlacesResponse } from '../interfaces/places';
import { MapService } from './map.service';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  // si se pone el signo de pregunta quiere decir si hay un valor lo ponga y si no entonces pone undefined
  public userLocation?: [number, number];

  public isLoadingPlaces: Boolean = false;

  public places: Feature[] = [];

  get isUserLocationReady(): Boolean {
    /* las dobles !! significa que no hay un valor y lo niega, si existe entonces va a retornar un true */
    return !!this.userLocation;
  }

  constructor(private placesPi: PlacesApiClient, private mapService: MapService) {
    this.getUserLocation();
  }

  /* metodo para saber cuando tengo la geolocalizacion */
  /*  este metodo nos va a regresar una promesa */
  public async getUserLocation(): Promise<[number, number]> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        /* desestructoramos y sacamos las coordenadas */
        ({ coords }) => {
          /* establecemos el valor al userLocation */
          this.userLocation = [coords.longitude, coords.latitude];
          resolve([coords.longitude, coords.latitude])
        },
        (error) => {
          alert('No se pudo obtener la geolocalizacion');
          console.log(error);
          /* ya termino la promesa */
          reject();
        }
      )
    });
  }

  getPlacesByQuery(query: string = '') {
    // evaludar cuando el query es un string vacio
    if (query.length === 0) {
      this.places = [];
      this.isLoadingPlaces = false;
      return;
    }

    this.isLoadingPlaces = true;

    if (!this.userLocation) throw Error('No hay userLocation');

    this.placesPi.get<PlacesResponse>(`/${query}.json?`, {
      params: {
        proximity: this.userLocation.join(',')
      }
    })
      .subscribe((response) => {
        this.isLoadingPlaces = false;
        this.places = response.features;
        this.mapService.createdMarkerFromPlaces(this.places, this.userLocation!);
      });


  }

  deletePlaces(){
    this.places = [];
  }



}
